#!/usr/bin/env bash

export EKS_PROD_1_INGRESS=$(kubectl --context ${EKS_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].hostname')
echo $EKS_PROD_1_INGRESS

sed -e s/EKS_PROD_1_INGRESS/${EKS_PROD_1_INGRESS}/g \
    serviceentry-in-gke-for-eks-redis-global.yaml_tmpl > serviceentry-in-gke-for-eks-redis-global.yaml

cat serviceentry-in-gke-for-eks-redis-global.yaml