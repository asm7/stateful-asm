#!/usr/bin/env bash

export GKE_PROD_1_INGRESS=$(kubectl --context ${GKE_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip')
echo $GKE_PROD_1_INGRESS

sed -e s/GKE_PROD_1_INGRESS/${GKE_PROD_1_INGRESS}/g \
    serviceentry-in-eks-for-gke-redis-global.yaml_tmpl > serviceentry-in-eks-for-gke-redis-global.yaml

cat serviceentry-in-eks-for-gke-redis-global.yaml