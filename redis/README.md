## Objective

You deploy Redis in $GKE_PROD_1 and $EKS_PROD_1 clusters. Ensure you have the variables defined. You can verify this by echoing the variable or by running the `source ./user_setup.sh` command from the workshop root folder.

## Prepare the yamls

1. Run the `build.sh` script in all `prod` clusters in their respective folders.

```bash
# From the `redis` folder
cd gke1
./build.sh

cd ../eks1
./build.sh

cd ../gke2
./build.sh

cd ../eks2
./build.sh
```

## Deploy redis

1. Run the following command to deploy Redis statefulsets in the GKE and EKS clusters.

```bash
cd ../gke1
kubectl ctx $GKE_PROD_1
kubectl apply -k .

cd ../eks1
kubectl ctx $EKS_PROD_1
kubectl apply -k .
```

1. Run the following command to create [ServiceEntries]() for the Redis statefulsets in the GKE and EKS clusters.

```bash
cd ../gke2
kubectl ctx $GKE_PROD_2
kubectl apply -k .

cd ../eks2
kubectl ctx $EKS_PROD_2
kubectl apply -k .
```

1. Ensure all Pods are up and _Running_.

```bash
kubectl --context=${GKE_PROD_1} -n db-redis get pods
kubectl --context=${EKS_PROD_1} -n db-redis get pods
```

Output (Do not copy)
```
+ kubectl --context=gke-prod-us-west2a-1 -n db-redis get pods
NAME          READY   STATUS    RESTARTS   AGE
gke-redis-0   2/2     Running   0          10m
gke-redis-1   2/2     Running   0          9m43s
gke-redis-2   2/2     Running   0          9m15s
+ kubectl --context=eks-prod-us-west2ab-1 -n db-redis get pods
NAME          READY   STATUS    RESTARTS   AGE
eks-redis-0   2/2     Running   0          9m52s
eks-redis-1   2/2     Running   0          9m31s
eks-redis-2   2/2     Running   0          8m57s
```

## Verify connectivity

1. Log in to one Redis pod from each of GKE and EKS cluster and perform `cluster nodes` from all nodes. You should see the appropriate IP:PORT as well as unique master IDs from all nodes.

```bash
##### From GKE to EKS #####

kubectl --context=${GKE_PROD_1} -n db-redis exec -it gke-redis-0 -- bash

redis-cli -h 240.0.0.20 cluster nodes           # this is gke-redis-0
redis-cli -h 240.0.0.21 cluster nodes           # this is gke-redis-1
redis-cli -h 240.0.0.22 cluster nodes           # this is gke-redis-2
redis-cli -h 240.0.0.10 cluster nodes           # this is eks-redis-0
redis-cli -h 240.0.0.11 cluster nodes           # this is eks-redis-1
redis-cli -h 240.0.0.12 cluster nodes           # this is eks-redis-2

exit

##### From EKS to GKE #####

kubectl --context=${EKS_PROD_1} -n db-redis exec -it eks-redis-0 -- bash

redis-cli -h 240.0.0.20 cluster nodes           # this is gke-redis-0
redis-cli -h 240.0.0.21 cluster nodes           # this is gke-redis-1
redis-cli -h 240.0.0.22 cluster nodes           # this is gke-redis-2
redis-cli -h 240.0.0.10 cluster nodes           # this is eks-redis-0
redis-cli -h 240.0.0.11 cluster nodes           # this is eks-redis-1
redis-cli -h 240.0.0.12 cluster nodes           # this is eks-redis-2

exit
```

## Create redis cluster

1. Log in to `gke-redis-0` node and run the following command to create a 6 node redis cluster.

```bash
kubectl --context=${GKE_PROD_1} -n db-redis exec -it gke-redis-0 -- bash

# Run the following command from the `gke-redis-0` Pod
redis-cli --cluster create --verbose --cluster-replicas 1 240.0.0.20:6379 240.0.0.21:6379 240.0.0.22:6379 240.0.0.10:6379 240.0.0.11:6379 240.0.0.12:6379
```

Output (Do not copy)
```
>>> Performing hash slots allocation on 6 nodes...
Master[0] -> Slots 0 - 5460
Master[1] -> Slots 5461 - 10922
Master[2] -> Slots 10923 - 16383
Adding replica 240.0.0.11:6379 to 240.0.0.20:6379
Adding replica 240.0.0.12:6379 to 240.0.0.21:6379
Adding replica 240.0.0.10:6379 to 240.0.0.22:6379
M: 1622ae26cd3650a1ffec63039ecbeae060233b74 240.0.0.20:6379
   slots:[0-5460] (5461 slots) master
M: ac266696a2a4101484e878a76e91f89cb2f8c101 240.0.0.21:6379
   slots:[5461-10922] (5462 slots) master
M: 76809c675fef0a2a67e7205d36187387198f371e 240.0.0.22:6379
   slots:[10923-16383] (5461 slots) master
S: 545455dac0e0bc5fe67ac4b8c9d95156215d4b7a 240.0.0.10:6379
   replicates 76809c675fef0a2a67e7205d36187387198f371e
S: 0f28a5dac7536eb6b6e6bb8883be2ee553a0d28e 240.0.0.11:6379
   replicates 1622ae26cd3650a1ffec63039ecbeae060233b74
S: c76be45049506228cd47bc203a6f2902859f5eb3 240.0.0.12:6379
   replicates ac266696a2a4101484e878a76e91f89cb2f8c101
Can I set the above configuration? (type 'yes' to accept): **yes**
```

1. Type `yes`.

Output (Do not copy)
```
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join

>>> Performing Cluster Check (using node 240.0.0.20:6379)
M: 1622ae26cd3650a1ffec63039ecbeae060233b74 240.0.0.20:6379
   slots:[0-5460] (5461 slots) master
   1 additional replica(s)
M: ac266696a2a4101484e878a76e91f89cb2f8c101 240.0.0.21:6379
   slots:[5461-10922] (5462 slots) master
   1 additional replica(s)
S: 0f28a5dac7536eb6b6e6bb8883be2ee553a0d28e 240.0.0.11:6379
   slots: (0 slots) slave
   replicates 1622ae26cd3650a1ffec63039ecbeae060233b74
S: c76be45049506228cd47bc203a6f2902859f5eb3 240.0.0.12:6379
   slots: (0 slots) slave
   replicates ac266696a2a4101484e878a76e91f89cb2f8c101
S: 545455dac0e0bc5fe67ac4b8c9d95156215d4b7a 240.0.0.10:6379
   slots: (0 slots) slave
   replicates 76809c675fef0a2a67e7205d36187387198f371e
M: 76809c675fef0a2a67e7205d36187387198f371e 240.0.0.22:6379
   slots:[10923-16383] (5461 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```