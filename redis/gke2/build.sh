#!/usr/bin/env bash

export GKE_PROD_1_INGRESS=$(kubectl --context ${GKE_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip')
echo $GKE_PROD_1_INGRESS

export EKS_PROD_1_INGRESS=$(kubectl --context ${EKS_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].hostname')
echo $EKS_PROD_1_INGRESS

sed -e s/EKS_PROD_1_INGRESS/${EKS_PROD_1_INGRESS}/g \
    serviceentry-in-gke-for-eks-redis-global.yaml_tmpl > serviceentry-in-gke-for-eks-redis-global.yaml

cat serviceentry-in-gke-for-eks-redis-global.yaml

sed -e s/GKE_PROD_1_INGRESS/${GKE_PROD_1_INGRESS}/g \
    serviceentry-in-gke2-for-gke1-redis-global.yaml_tmpl > serviceentry-in-gke2-for-gke1-redis-global.yaml

cat serviceentry-in-gke2-for-gke1-redis-global.yaml