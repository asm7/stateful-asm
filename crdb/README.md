# CockroachDB

## Objective

Deploy `cockroachdb` as statfulsets in a GKE and EKS cluster in the `db-crdb` namespace. Each statefulset has three Pods for a total of six Pods: 3 Pods running in GKE and 3 Pods running in EKS.

Every Pod must be able to access every other Pod via Pod FQDN (in the form of `pod_name.headless_svc.namespace.svc.cluster.local`).

## Creating and labeling the `db-crdb` namespace

1.  Define vars for your cluster contexts.

    ```bash
    export GKE=<GKE CLUSTER CONTEXT>
    export EKS=<EKS CLUSTER CONTEXT>
    ```

1.  Create the `db-crdb` namespace in both GKE and EKS clusters.

    ```bash
    kubectl --context=$GKE create ns db-crdb
    kubectl --context=$EKS create ns db-crdb
    ```

1.  Get the correct ASM label for auto sidecar injection. You can get this from any one of the two clusters (they should be the same).

    ```bash
    kubectl --context=$GKE -n istio-system get pods -l app=istiod --show-labels
    ```

Get the label that looks like `istio.io/rev=asm-183-2`. Your label might be slightly different depending upon the version.

1. Label the namespaces in both clusters.

   ```bash
   kubectl --context=$GKE label ns db-crdb istio.io/rev=asm-183-2
   kubectl --context=$EKS label ns db-crdb istio.io/rev=asm-183-2
   ```

1. For Istio OSS, use the following label.

   ```bash
   kubectl --context=$GKE label ns db-crdb istio-injection=enabled
   kubectl --context=$EKS label ns db-crdb istio-injection=enabled
   ```

## Deploying the stateful sets and services

1. Create the statefulsets in both clusters.

   ```bash
   kubectl --context=$GKE -n db-crdb apply -f statefulset--gke-crdb.yaml
   kubectl --context=$EKS -n db-crdb apply -f statefulset--eks-crdb.yaml
   ```

1. Create the `crdb` Service.

   ```bash
   kubectl --context=$GKE -n db-crdb apply -f service--crdb.yaml
   kubectl --context=$EKS -n db-crdb apply -f service--crdb.yaml
   ```

The output should be similar to the following.

    partitioned roll out complete: 3 new pods have been updated...

## ServiceEntries

1.  Create a ServiceEntry in the GKE to create DNS entries for all Pods.

    ```bash
    kubectl --context=$GKE -n db-crdb apply -f serviceentry--crdb.yaml
    kubectl --context=$EKS -n db-crdb apply -f serviceentry--crdb.yaml
    ```

1.  Create the `cluster-init` job to create the CockroachDB cluster.

    ```bash
    kubectl --context=$GKE -n db-crdb apply -f job--cluster-init.yaml
    ```

1.  Expose one of the CRDB Pods and access the UI. Click the **Network Latency** link in the left navbar. Check node to node latency.

    ```bash
    kubectl --context=${GKE} -n db-crdb port-forward gke-crdb-0 8080:8080 --address 0.0.0.0
    ```

1.  Check the proxy config endpoint configs in both cluster. Check the GKE cluster.

    ```bash
    istioctl --context=$GKE -n db-crdb pc ep gke-crdb-0 | grep db-crdb
    ```

Output looks like the following:

    10.0.0.14:8080                   HEALTHY     OK                outbound|8080||gke-crdb-2.gke-crdb.db-crdb.svc.cluster.local
    10.0.0.14:26257                  HEALTHY     OK                outbound|26257||gke-crdb-2.gke-crdb.db-crdb.svc.cluster.local
    10.0.1.9:8080                    HEALTHY     OK                outbound|8080||gke-crdb-0.gke-crdb.db-crdb.svc.cluster.local
    10.0.1.9:26257                   HEALTHY     OK                outbound|26257||gke-crdb-0.gke-crdb.db-crdb.svc.cluster.local
    10.0.3.6:8080                    HEALTHY     OK                outbound|8080||gke-crdb-1.gke-crdb.db-crdb.svc.cluster.local
    10.0.3.6:26257                   HEALTHY     OK                outbound|26257||gke-crdb-1.gke-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|26257||eks-crdb-0.eks-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|26257||eks-crdb-1.eks-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|26257||eks-crdb-2.eks-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|8080||eks-crdb-0.eks-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|8080||eks-crdb-1.eks-crdb.db-crdb.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|8080||eks-crdb-2.eks-crdb.db-crdb.svc.cluster.local

The remote Pods show the remote clusters `istio-eastwestgateway` load balancer IP address. You can confirm this by inspecting the IP address of the EKS cluster's `istio-eastwestgateway` load balancer IP address.

```bash
kubectl --context=$EKS -n istio-system get svc istio-eastwestgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}'
```

Output looks like the following.

    34.122.66.140

1.  Check the proxy configs from the EKS cluster.

    ```bash
    istioctl --context=$EKS -n db-crdb pc ep eks-crdb-0 | grep db-crdb
    ```

Output looks like the following:

    10.0.0.7:8080                    HEALTHY     OK                outbound|8080||eks-crdb-1.eks-crdb.db-crdb.svc.cluster.local
    10.0.0.7:26257                   HEALTHY     OK                outbound|26257||eks-crdb-1.eks-crdb.db-crdb.svc.cluster.local
    10.0.1.7:8080                    HEALTHY     OK                outbound|8080||eks-crdb-2.eks-crdb.db-crdb.svc.cluster.local
    10.0.1.7:26257                   HEALTHY     OK                outbound|26257||eks-crdb-2.eks-crdb.db-crdb.svc.cluster.local
    10.0.2.12:8080                   HEALTHY     OK                outbound|8080||eks-crdb-0.eks-crdb.db-crdb.svc.cluster.local
    10.0.2.12:26257                  HEALTHY     OK                outbound|26257||eks-crdb-0.eks-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|26257||gke-crdb-0.gke-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|26257||gke-crdb-1.gke-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|26257||gke-crdb-2.gke-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|8080||gke-crdb-0.gke-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|8080||gke-crdb-1.gke-crdb.db-crdb.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|8080||gke-crdb-2.gke-crdb.db-crdb.svc.cluster.local

The remote Pods show the remote clusters `istio-eastwestgateway` load balancer IP address. You can confirm this by inspecting the IP address of the EKS cluster's `istio-eastwestgateway` load balancer IP address.

```bash
kubectl --context=$GKE -n istio-system get svc istio-eastwestgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}'
```

Output looks like the following.

    35.236.97.106

## Importing test data

[Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos) is a sample application used for demoing the Anthos platform. There are two PostgreSQL DBs as part of this application. These databases contain sample data. The `cockroachdb` repository contains SQL dumps from the Bank of Anthos databases using [pg_dump](https://www.cockroachlabs.com/docs/stable/migrate-from-postgres.html#dump-the-entire-database). You can import this data to cockroachdb.

1. Copy the SQL dump files to one of the cockroachdb nodes.

   ```bash
   kubectl --context=$GKE -n db-crdb exec -t gke-crdb-0 -- mkdir -p /cockroach/cockroach-data/extern
   kubectl --context=$GKE -n db-crdb cp dump-accounts-db.sql gke-crdb-0:/cockroach/cockroach-data/extern/dump-accounts-db.sql
   kubectl --context=$GKE -n db-crdb cp dump-postgresdb.sql gke-crdb-0:/cockroach/cockroach-data/extern/dump-postgresdb.sql
   ```

1. Log in to the `gke-crdb-0` node.

   ```bash
   kubectl --context=$GKE -n db-crdb exec -it gke-crdb-0 -- cockroach sql --insecure --host=crdb
   ```

1. Create two databases. Bank of Anthos app uses two databases named `accountsdb` and `postgresdb`.

   ```sql
   CREATE DATABASE accountsdb;
   CREATE DATABASE postgresdb;
   ```

1. Import data using the SQL dump files.

   ```sql
   -- Import DBs
   USE accountsdb;
   IMPORT PGDUMP 'nodelocal://1/dump-accounts-db.sql';
   SHOW TABLES;
   SELECT * FROM contacts;
   SELECT * FROM users;

   USE postgresdb;
   IMPORT PGDUMP 'nodelocal://1/dump-postgresdb.sql';
   SHOW TABLES;
   SELECT * FROM transactions;
   ```

1. Exit out of the node.

   ```bash
   \q
   ```
