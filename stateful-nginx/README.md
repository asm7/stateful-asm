# Stateful NGINX

## Objective

Deploy `nginx` as statfulsets in a GKE and EKS cluster in the `web` namespace. Each statefulset has two Pods for a total of four Pods: 2 Pods running in GKE and 2 Pods running in EKS.

Every Pod must be able to access every other Pod via Pod FQDN (in the form of `pod_name.headless_svc.namespace.svc.cluster.local`) and IP address.

Certain databases like CockroachDB uses DNS names to access all nodes. While other databases like Redis require IP address connectivity between all nodes.

Using `nginx` makes it easy to use curl to ensure the right Pod is being connected to.

## Creating and labeling the `web` namespace

1.  Define vars for your cluster contexts.

    ```bash
    export GKE=<GKE CLUSTER CONTEXT>
    export EKS=<EKS CLUSTER CONTEXT>
    ```

1.  Create the `web` namespace in both GKE and EKS clusters.

    ```bash
    kubectl --context=$GKE create ns web
    kubectl --context=$EKS create ns web
    ```

1.  Get the correct ASM label for auto sidecar injection. You can get this from any one of the two clusters (they should be the same).

    ```bash
    kubectl --context=$GKE -n istio-system get pods -l app=istiod --show-labels
    ```

Get the label that looks like `istio.io/rev=asm-183-2`. Your label might be slightly different depending upon the version.

1. Label the namespaces in both clusters.

    ```bash
    kubectl --context=$GKE label ns web istio.io/rev=asm-183-2
    kubectl --context=$EKS label ns web istio.io/rev=asm-183-2
    ```

1. For Istio OSS, use the following label.

    ```bash
    kubectl --context=$GKE label ns web istio-injection=enabled
    kubectl --context=$EKS label ns web istio-injection=enabled
    ```

## Deploying the stateful sets and services

1. Create the statefulsets in both clusters.

    ```bash
    kubectl --context=$GKE -n web apply -f statefulset--web-gke.yaml
    kubectl --context=$EKS -n web apply -f statefulset--web-eks.yaml
    ```

1. Confirm that all Pods are running.

    ```bash
    kubectl --context=$GKE -n web rollout status --watch --timeout=600s statefulset/gke-web
    kubectl --context=$EKS -n web rollout status --watch --timeout=600s statefulset/eks-web
    ```

The output should be similar to the following.

    partitioned roll out complete: 2 new pods have been updated...

## Configuring `nginx`

1. Configure the statefulset Pods with a simple HTML file so that you can identify them during curl/connectivity testing.

    ```bash
    kubectl --context=$GKE -n web exec -it gke-web-0 -c nginx -- bash -c "echo gke-web-0 > /usr/share/nginx/html/index.html"
    kubectl --context=$GKE -n web exec -it gke-web-1 -c nginx -- bash -c "echo gke-web-1 > /usr/share/nginx/html/index.html"
    kubectl --context=$EKS -n web exec -it eks-web-0 -c nginx -- bash -c "echo eks-web-0 > /usr/share/nginx/html/index.html"
    kubectl --context=$EKS -n web exec -it eks-web-1 -c nginx -- bash -c "echo eks-web-1 > /usr/share/nginx/html/index.html"
    ```

## ServiceEntries

1. Create a ServiceEntry in the GKE to create DNS entries for all Pods.

    ```bash
    kubectl --context=$GKE -n web apply -f serviceentry--web-dns.yaml
    kubectl --context=$EKS -n web apply -f serviceentry--web-dns.yaml
    ```

1.  Check the proxy configs endpoints and make sure all four Pods show up in both clusters.

    ```bash
    istioctl --context=$GKE -n web pc ep gke-web-0 | grep nginx
    ```

Output looks like the following:

    10.0.0.9:80                      HEALTHY     OK                outbound|80||gke-web-0.nginx.web.svc.cluster.local
    10.0.3.5:80                      HEALTHY     OK                outbound|80||gke-web-1.nginx.web.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|80||eks-web-0.nginx.web.svc.cluster.local
    34.122.66.140:15443              HEALTHY     OK                outbound|80||eks-web-1.nginx.web.svc.cluster.local

The remote Pods show the remote clusters `istio-eastwestgateway` load balancer IP address. You can confirm this by inspecting the IP address of the EKS cluster's `istio-eastwestgateway` load balancer IP address.

```bash
kubectl --context=$EKS -n istio-system get svc istio-eastwestgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}'
```

Output looks like the following.

    34.122.66.140

1.  Check the proxy configs from the EKS cluster.

    ```bash
    istioctl --context=$EKS -n web pc ep eks-web-0 | grep nginx
    ```

Output looks like the following:

    10.0.1.5:80                      HEALTHY     OK                outbound|80||eks-web-1.nginx.web.svc.cluster.local
    10.0.2.5:80                      HEALTHY     OK                outbound|80||eks-web-0.nginx.web.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|80||gke-web-0.nginx.web.svc.cluster.local
    35.236.97.106:15443              HEALTHY     OK                outbound|80||gke-web-1.nginx.web.svc.cluster.local

The remote Pods show the remote clusters `istio-eastwestgateway` load balancer IP address. You can confirm this by inspecting the IP address of the EKS cluster's `istio-eastwestgateway` load balancer IP address.

```bash
kubectl --context=$GKE -n istio-system get svc istio-eastwestgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}'
```

Output looks like the following.

    35.236.97.106

1. Access all Pods from all Pods using Pod FQDNs.

    ```bash
    for CTX in $GKE $EKS
    do
        for GKE_POD in gke-web-0 gke-web-1
        do
            for EKS_POD in eks-web-0 eks-web-1
            do
                if [ $CTX = $GKE ]; then FROM_POD=$GKE_POD && TO_POD=$EKS_POD; else FROM_POD=$EKS_POD && TO_POD=$GKE_POD; fi
                echo -e "\e[92mFrom $FROM_POD to $TO_POD...\e[0m"
                kubectl --context=$CTX -n web exec -i $FROM_POD -c nginx -- curl -s $TO_POD.nginx.web.svc.cluster.local
            done
        done
    done
    ```

1. Access all Pods from all Pods using Pod IPs (as configured in the ServiceEntries).

    ```bash
    for CTX in $GKE $EKS
    do
        for GKE_POD in gke-web-0 gke-web-1
        do
            for EKS_POD in eks-web-0 eks-web-1
            do
                if [ $CTX = $GKE ]; then
                    FROM_POD=$GKE_POD
                    if [ $EKS_POD = eks-web-0 ]; then TO_POD_IP="240.0.0.20"; TO_POD=eks-web-0; fi
                    if [ $EKS_POD = eks-web-1 ]; then TO_POD_IP="240.0.0.21"; TO_POD=eks-web-0; fi
                else
                    FROM_POD=$EKS_POD
                    if [ $GKE_POD = gke-web-0 ]; then TO_POD_IP="240.0.0.10"; TO_POD=gke-web-0; fi
                    if [ $GKE_POD = gke-web-1 ]; then TO_POD_IP="240.0.0.11"; TO_POD=gke-web-1; fi
                fi
                echo -e "\e[92mFrom $FROM_POD to $TO_POD...\e[0m"
                kubectl --context=$CTX -n web exec -i $FROM_POD -c nginx -- curl -s $TO_POD_IP
            done
        done
    done
    ```
